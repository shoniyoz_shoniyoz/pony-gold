const data = [
  {
    name: "Все заказы",
    url: "/",
    icon: "fa fa-bag-shopping",
  },
  {
    name: "Статистика",
    url: "/statistics",
    icon: "fa fa-chart-pie",
  },
  {
    name: "Партнеры",
    url: "/partners",
    icon: "fa fa-users",
  },
  {
    name: "Пользователи",
    url: "/users",
    icon: "fa fa-user",
  },
  {
    name: "Отчеты по Pony Gold",
    url: "/reports",
    icon: "fa fa-square-poll-vertical",
  },
  {
    name: "Товары",
    url: "/products",
    icon: "fa fa-tag",
  },
  {
    name: "Бренды",
    url: "/brands",
    icon: "fa fa-infinity",
  },
  {
    name: "История действий",
    url: "/history",
    icon: "fa fa-clock-rotate-left",
  },
  // {
  //   name: "Баннеры",
  //   url: "/banner",
  //   icon: "fa fa-clock-rotate-left",
  // },
  // {
  //   name: "Категории",
  //   url: "/categories",
  //   icon: "fa fa-clock-rotate-left",
  // },
];
export default data;
