import { defineStore } from "pinia";
import axios from "axios";
import { reactive } from "vue";

const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL,
});

export const useStore = defineStore("main", () => {
  const data = reactive<{ [key: string]: any }>({});
  const fetchAction = async (
    url: string,
    keyState: string,
    params: { offset?: number; page: number }
  ) => {
    return new Promise((resolve, reject) => {
      $axios
        .get(url, {
          params,
        })
        .then((response) => {
          if (data[keyState] == undefined) {
            data[keyState] = response.data;
          } else {
            if (params?.offset !== 0) {
              data[keyState].results.push(...response.data.results);
            } else {
              data[keyState] = response.data;
            }
          }
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  };

  const fetchActionWithoutParams = async (url: string, keyState: string) => {
    return new Promise((resolve, reject) => {
      $axios
        .get(url)
        .then((res) => {
          data[keyState] = res.data;
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const postAction = async (url: string, form: any) => {
    return new Promise((resolve, reject) => {
      $axios
        .post(url, {
          ...form,
        })
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  return {
    data,
    fetchAction,
    fetchActionWithoutParams,
    postAction,
  };
});
