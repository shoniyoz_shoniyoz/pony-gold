export default [
  {
    path: "/:pathMatch(.*)*",
    meta: { title: "Error", layout: "empty" },
    component: () => import("@/layouts/LEmpty.vue"),
  },
  {
    path: "/login",
    name: "PLogin",
    meta: { title: "PLogin", layout: "login" },
    component: () => import("@/pages/PLogin/Index.vue"),
  },
  {
    path: "/",
    name: "POrders",
    meta: { title: "POrders", layout: "default" },
    component: () => import("@/pages/POrders/Index.vue"),
  },
  {
    path: "/statistics",
    name: "PStatistics",
    meta: { title: "PStatistics", layout: "default" },
    component: () => import("@/pages/PStatistics/Index.vue"),
  },
  {
    path: "/partners",
    name: "PPartners",
    meta: { title: "PPartners", layout: "default" },
    component: () => import("@/pages/PPartners/Index.vue"),
  },
  {
    path: "/partners/create",
    name: "Create",
    meta: { title: "Create", layout: "default" },
    component: () => import("@/pages/PPartners/Create.vue"),
  },
  {
    path: "/users",
    name: "PUsers",
    meta: { title: "PUsers", layout: "default" },
    component: () => import("@/pages/PUsers/Index.vue"),
  },
  {
    path: "/reports",
    name: "PReports",
    meta: { title: "PReports", layout: "default" },
    component: () => import("@/pages/PReports/Index.vue"),
  },
  {
    path: "/products",
    name: "PProducts",
    meta: { title: "PProducts", layout: "default" },
    component: () => import("@/pages/PProducts/Index.vue"),
  },
  {
    path: "/brands",
    name: "PBrands",
    meta: { title: "PBrands", layout: "default" },
    component: () => import("@/pages/PBrands/Index.vue"),
  },
  {
    path: "/history",
    name: "PHistory",
    meta: { title: "PHistory", layout: "default" },
    component: () => import("@/pages/PHistory/Index.vue"),
  },
];
